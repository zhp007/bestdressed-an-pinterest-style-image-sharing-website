### BestDressed: A Pinterest-style Dress Image Sharing Website ###

* Built an image sharing website using MEAN Stack - MongoDB, Express.js, AngularJs and Node.js. 
* Built web pages using HTML, CSS, JavaScript and Bootstrap. Created the Pinterest style view using Angular Grid.
* Built RESTful APIs using Node.js for image CRUD operations, web scraping, etc.
* Deployed the App on AWS. Deployed an Elastic Load Balancer behind 3 AWS EC2 instances to increase website availability. Stored the images in AWS S3. Increased 20% of image delivery speed using AWS CloudFront CDN.

### Demo URL ###
* bestdressedelb-1591570758.us-west-2.elb.amazonaws.com
* Support Chrome and Firefox

### Run Configuration ###

* Please modify MongolabURI, S3 bucket secret, etc.
* change to development mode
* run command "npm install", "bower intall"
* navigate to "/server", type command "node app.js"