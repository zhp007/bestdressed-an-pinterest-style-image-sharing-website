'use strict';

// get the html code
var request = require('request');   
// parse html, like jQuery selector
var cheerio = require('cheerio'); 

exports.list = function(url, cb) {
  request(url, function(error, resp, body) {
    if(error) {
      cb({
        error: error
      });
    }
    if(!error){
      var $ = cheerio.load(body);
      var pin = {};
      var $url = url;
      var $img = $('.imgTagWrapper img').attr('src');
      var $desc = $('.imgTagWrapper img').attr('alt');

      console.log($img + ' pin url');

      var pin = {
        img: $img,
        url: $url,
        desc: $desc
      }

      // respond with the final JSON object
      cb(pin);
    }
  });
}