'use strict';

var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');

var scrapers = {};

scrapers['pinterest'] = require('./scrapers/pinterest.js');
scrapers['ebay'] = require('./scrapers/ebay.js');
scrapers['amazon'] = require('./scrapers/amazon.js');
scrapers['groupon'] = require('./scrapers/groupon.js');

exports.scrape = function(req, res) {
  var url = req.body.url;
  var scraperToUse;

  if (url.indexOf("pinterest") > -1) {
    scraperToUse = 'pinterest';
  } else if(url.indexOf("ebay") > -1) {
    scraperToUse = 'ebay';  
  } else if(url.indexOf("amazon") > -1) {
    scraperToUse = 'amazon';  
  } else if(url.indexOf("groupon") > -1) {
    scraperToUse = 'groupon';
  } else {
    console.log('cannot locate scraper');
  }

  scrapers[scraperToUse].list(url, function(data) {
    console.log('data from scraper: ', data);
    res.json(data);
  });
}
