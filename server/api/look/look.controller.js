'use strict';

var _ = require('lodash');
var Look = require('./look.model');
var path = require('path');
var express = require('express');
var utils = require('../../utils/utils.js');

// knox to upload file to s3
var config = require('../../config/environment');
var knox = require('knox');
var fs = require('fs');

var knoxClient = knox.createClient({
    key: config.s3keys.access,
    secret: config.s3keys.secret,
    bucket: config.s3keys.bucket
});

exports.allLooks = function(req, res) {
  Look.find({})
    .sort({
      createTime: -1
    })
    .exec(function(err, looks) {
      if (err) {
        return handleError(res, err);
      }
      if (!looks) {
        return res.send(404);
      }
      console.log(looks);
      return res.status(200)
                     .json(looks);
    });
};

exports.userLooks = function(req, res) {
  var userEmail = req.query.email;
  Look.find({
    email: {
      $in: userEmail
    }
  })
  .sort({
    createTime: -1
  })
  .exec(function(err, looks) {
    if(err) {
      return handleError(res, err);
    }
    console.log(looks);
    return res.status(200)
                   .json(looks);
  });
};

exports.scrapeUpload = function(req, res) {
  var random = utils.randomizer(32, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');

  utils.downloadURI(req.body.image, '../client/assets/images/uploads/' + random + '.png', 
  function(filename) {
    var date = new Date();
    var s3filename = random + "-" + date.getTime() + '.png';
    fs.readFile(filename, function(err, buf) {
        var req = knoxClient.put(s3filename, {
            'Content-Length': buf.length,
            'Content-Type': 'image/png'
        });
        req.on('response', function(res) {
            if(res.statusCode == 200) {
                console.log('done');
            }
        })
        req.end(buf);
    });  

    var newLook = new Look();
    newLook.title = req.body.title;
    // newLook.image = filename.slice(9);
    newLook.image = 'https://s3.amazonaws.com/bestdressed001/' + s3filename;
    newLook.email = req.body.email;
    newLook.linkURL = req.body.linkURL;
    newLook.description = req.body.description;
    newLook.userName = req.body.name;
    newLook._creator = req.body._creator;
    newLook.createTime = Date.now();
    newLook.upVotes = 0;
    newLook.save(function(err, item) {
      if (err) {
        console.log('error occured in saving post');
      } else {
        console.log('Success post saved');
        console.log(item);
        /*
        res.status(200)
          .json(item);
        */
        setTimeout(function() {
          res.status(200)
            .json(item);            
        }, 1000);
      }
    });
  });
}

exports.upload = function(req, res) {
  var newLook = new Look();
  
  var fileimage = req.middlewareStorage.fileimage;
  var filename = '../client/assets/images/uploads/' + fileimage;
  fs.readFile(filename, function(err, buf) {
      var req = knoxClient.put(fileimage, {
        'Content-Length': buf.length,
        'Content-Type': 'image/png'
      });
      req.on('response', function(res) {
          if(res.statusCode == 200) {
              console.log('done');
          }
      })
      req.end(buf);
  });    

  console.log(req.body);
  newLook.image = 'https://s3.amazonaws.com/bestdressed001/' + fileimage;
  newLook.email = req.body.email;
  newLook.linkURL = req.body.linkURL;
  newLook.title = req.body.title;
  newLook.description = req.body.description;
  newLook.userName = req.body.name;
  newLook._creator = req.body._creator;
  newLook.createTime = Date.now();
  newLook.upVotes = 0;

  newLook.save(function(err, look) {
    if(err) {
      console.log('error saving look');
      return res.send(500);
    } else {
      console.log(look);
      /*
      res.status(200)
           .send(look);
      */
      setTimeout(function() {
        res.status(200)
          .send(look);            
      }, 1000);
    }
  });
};

exports.singleLook = function(req, res) {
  Look.findById(req.params.lookId, function(err, look) {
    if(err) {
      return handleError(res, err);
    }
    if(!look) {
      return res.send(404);
    }
    return res.json(look);
  });
};

exports.popLooks = function(req, res) {
  Look.find(req.params.id)
    .sort('-upVotes')
    .limit(6)
    .exec(function(err, looks) {
      if (err) {
        return handleError(res, err);
      }
      console.log(looks);
      return res.json(looks);
    });
}

exports.update = function(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  Look.findById(req.params.id, function(err, look) {
    if(err) {
      return handleError(res, err);
      }
      if(!look) {
        return res.send(404);
      }
      var updated = _.merge(look, req.body);
      updated.save(function(err) {
        if(err) {
          return handleError(res, err);
        }
        console.log(look);
        return res.json(look);
      });
  });
};

exports.delete = function(req, res) {
  Look.findById(req.params.id, function(err, look) {
    if(err) {
      return handleError(res, err);
    }
    if(!look) {
      return res.send(404);
    }
    look.remove(function(err) {
      if(err) {
        return handleError(res, err);
      }
      return res.send(200);
    });
  });
};

exports.addView = function(req, res) {
  Look.findById(req.params.id, function(err, look) {
    if(err) {
      return handleError(res, err);
    }
    if (!look) {
      return res.send(404);
    }
    look.views++;
    look.save(function(err) {
      if (err) {
        return handle(res, err);
      }
      return res.json(look);
    });
  });
};

exports.addUpvote = function(req, res) {
  Look.findById(req.params.id, function(err, look) {
    if(err) {
      return handleError(res, err);
    }
    if(!look) {
      return res.send(404);
    }
    look.upVotes++;
    look.save(function(err) {
      if(err) {
        return handleError(res, err);
      }
      return res.json(look);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}